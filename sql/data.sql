Insert into Brokers
    (Ic, Title, DateStart, ContractEnd, DataFile)
values
    (1, 'Broker1', Convert(varchar(30),'1/1/2018',102), 0, null);
Insert into Brokers
    (Ic, Title, DateStart, ContractEnd, DataFile)
values
    (2, 'Broker2', Convert(varchar(30),'2/1/2018',102), 1, null);
Insert into Brokers
    (Ic, Title, DateStart, ContractEnd, DataFile)
values
    (3, 'Broker3', Convert(varchar(30),'3/1/2018',102), 1, null);
Insert into Brokers
    (Ic, Title, DateStart, ContractEnd, DataFile)
values
    (4, 'Broker4', Convert(varchar(30),'4/1/2018',102), 0, null);
Insert into Brokers
    (Ic, Title, DateStart, ContractEnd, DataFile)
values
    (5, 'Broker5', Convert(varchar(30),'5/1/2018',102), 1, null);

Insert into Users
    (PhoneNumber, BirthNumber, FirstName, Surname, Email, Note)
values(111111111, '875321/3240', 'Jana', 'Sucha', 'jana.sucha@gmail.com', null);
Insert into Users
    (PhoneNumber, BirthNumber, FirstName, Surname, Email, Note)
values(222222222, '840321/3240', 'Pavel', 'Maly', 'pavel.maly@gmail.com', null);
Insert into Users
    (PhoneNumber, BirthNumber, FirstName, Surname, Email, Note)
values(333333333, '885321/3370', 'Ondrej', 'Suuda', 'ondrej.suda@gmail.com', null);
Insert into Users
    (PhoneNumber, BirthNumber, FirstName, Surname, Email, Note)
values(444444444, '715426/7589', 'Pavla', 'Nova', 'pavla.nova@gmail.com', null);
Insert into Users
    (PhoneNumber, BirthNumber, FirstName, Surname, Email, Note)
values(555555555, '870101/2567', 'Vaclav', 'Sojka', 'vaclav.sojka@gmail.com', null);
Insert into Users
    (PhoneNumber, BirthNumber, FirstName, Surname, Email, Note)
values(666666666, '910622/3287', 'Jan', 'Pavel', 'jan.pavel@gmail.com', null);
Insert into Users
    (PhoneNumber, BirthNumber, FirstName, Surname, Email, Note)
values(777777777, '920904/7789', 'Martin', 'Stengl', 'martin.stengl@gmail.com', null);

Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (1, 111111111);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (2, 111111111);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (5, 111111111);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (1, 222222222);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (4, 222222222);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (3, 222222222);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (5, 222222222);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (2, 333333333);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (2, 333333333);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (1, 333333333);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (1, 444444444);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (5, 444444444);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (3, 555555555);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (2, 555555555);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (1, 666666666);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (3, 666666666);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (5, 666666666);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (3, 777777777);
Insert into BrokerUser
    (BrokerIc, UserPhone)
values
    (4, 777777777);

Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (111111111, null, 1);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (111111111, null, 2);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (111111111, 1, 3);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (222222222, null, 1);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (222222222, null, 1);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (333333333, null, 1);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (333333333, null, 1);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (333333333, 2, 3);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (444444444, null, 1);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (444444444, null, 1);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (555555555, null, 2);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (555555555, null, 2);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (555555555, 3, 3);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (666666666, null, 1);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (666666666, null, 1);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (666666666, null, 1);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (777777777, 4, 3);
Insert into ContactAttempts
    (UserId, LoanId, ContactResult)
values
    (777777777, 5, 3);

Insert into Loans
    (DueDate, UserId, Amount, BrokerIc, Contracted, MonthlyPayment)
values
    (6, 111111111, 100000, 1, 1, 20000);
Insert into Loans
    (DueDate, UserId, Amount, BrokerIc, Contracted, MonthlyPayment)
values
    (10, 333333333, 100000, 2, 1, 12000);
Insert into Loans
    (DueDate, UserId, Amount, BrokerIc, Contracted, MonthlyPayment)
values
    (12, 555555555, 200000, 3, 1, 20000);
Insert into Loans
    (DueDate, UserId, Amount, BrokerIc, Contracted, MonthlyPayment)
values
    (6, 777777777, 300000, 1, 1, 60000);
Insert into Loans
    (DueDate, UserId, Amount, BrokerIc, Contracted, MonthlyPayment)
values
    (6, 777777777, 100000, 2, 1, 21000);
