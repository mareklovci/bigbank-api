create table Brokers
(
    Ic integer primary key,
    Title varchar(30),
    DateStart datetime,
    ContractEnd bit,
    DataFile varchar(30)
);

create table BrokerUser
(
    Id integer IDENTITY(1,1) primary key,
    BrokerIc integer,
    UserPhone integer
);

create table Users
(
    PhoneNumber integer primary key,
    BirthNumber varchar(12),
    FirstName varchar(20),
    Surname varchar(30),
    Email varchar(30),
    Note text
);

alter table BrokerUser add constraint fk_Broker_BrokerUser 
foreign key (BrokerIc) references Brokers (Ic);

alter table BrokerUser add constraint fk_User_BrokerUser 
foreign key (UserPhone) references Users (PhoneNumber);

create table ContactAttempts
(
    Id integer IDENTITY(1,1) primary key,
    UserId integer,
    LoanId integer,
    ContactResult integer
);

alter table ContactAttempts add constraint fk_User_contactAttempt 
foreign key (UserId) references Users (PhoneNumber);

create table Loans
(
    Id integer IDENTITY(1,1) primary key,
    DueDate bigint,
    UserId integer,
    Amount integer,
    BrokerIc integer,
    Contracted bit,
    MonthlyPayment integer
);

alter table Loans add constraint fk_Loan_Users
foreign key (UserId) references Users (PhoneNumber);
