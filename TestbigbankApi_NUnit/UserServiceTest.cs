﻿using bigbank_api.Controllers;
using NUnit.Framework;
using System;


namespace TestbigbankApi_NUnit
{
    /// <summary>
    /// Test class
    /// </summary>
    [TestFixture]
    class UserServiceTest
    {
        private UserService userService;

        [SetUp]
        public void Initialize()
        {
            userService = new UserService();
        }

        /// <summary>
        /// Tests the monthly payment.
        /// </summary>

        [Test]
        public void TestMonthlyPayment()
        {
            //userService.CalculatePayment(100000, 6);
            Assert.That(userService.CalculatePayment(100000, 6), Is.EqualTo(20000));
        }
    }
}
