﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title>Contact report</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
      </head>
      <body>
        <div class="container">
          <h1>
            Contact Report
          </h1>
          <table class="table table-striped">
            <thead>

            </thead>
            <tbody>
              <xsl:for-each select="ArrayOfBrokerStat/BrokerStat">
                <tr>
                  <td>
                    <h2>
                      <xsl:value-of select="BrokerName" />
                    </h2>
                  </td>
                  <td>

                  </td>
                </tr>
                <tr>
                  <td>
                    Count of contact attempts
                  </td>
                  <td>
                    <xsl:value-of select="CountOfContactAttempts"/>
                  </td>
                </tr>
                <tr>
                  <td>
                    Count of successful contact attempts
                  </td>
                  <td>
                    <xsl:value-of select="CountOfSuccessfulAttempts"/>
                  </td>
                </tr>
                <tr>
                  <td>
                    Count of unsuccessful contact attempts
                  </td>
                  <td>
                    <xsl:value-of select="CountOfUnsuccessfulAttempts"/>
                  </td>
                </tr>
              </xsl:for-each>
            </tbody>
          </table>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
