using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using Newtonsoft.Json;

namespace XmlTransform
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlReader reader = XmlReader.Create(@"../../contact_report.xml");
            XmlWriter writer = XmlWriter.Create(@"../../contact_report.html");

            XslCompiledTransform transform = new XslCompiledTransform();
            XsltSettings settings = new XsltSettings();
            settings.EnableScript = true;

            transform.Load(@"../../transform.xslt", settings, null);

            transform.Transform(reader, writer);
        }
    }
}
