namespace bigbank_api.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ContactAttempt
    {
        /// <summary>
        /// Get and set methods
        /// </summary>
        public int Id { get; set; }

        public int? UserId { get; set; }

        public int? LoanId { get; set; }

        public int? ContactResult { get; set; }

        public virtual User User { get; set; }
    }
}
