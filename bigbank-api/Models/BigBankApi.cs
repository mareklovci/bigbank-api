namespace bigbank_api.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// class BigBankApi and connection with DB
    /// </summary>
    /// <seealso cref="System.Data.Entity.DbContext" />
    public partial class BigBankApi : DbContext
    {
        public BigBankApi()
            : base("name=BigBankApi")
        {
        }

        /// <summary>
        /// Get and set methods
        /// </summary>
        public virtual DbSet<Broker> Brokers { get; set; }
        public virtual DbSet<ContactAttempt> ContactAttempts { get; set; }
        public virtual DbSet<Loan> Loans { get; set; }
        public virtual DbSet<User> Users { get; set; }


        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Broker>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Broker>()
                .Property(e => e.DataFile)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.BirthNumber)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ContactAttempts)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Loans)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.UserId);
        }
    }
}
