﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bigbank_api.Models
{
    /// <summary>
    /// Enum for contact results
    /// </summary>
    public class ContactResult
    {
        enum ContactResultType { NotContacted = 1, ContactedAndContracted = 2, ContactedAndNotContracted = 3 };
    }
}