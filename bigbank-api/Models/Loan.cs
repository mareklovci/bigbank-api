namespace bigbank_api.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Loan
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        public long? DueDate { get; set; }

        public int? UserId { get; set; }

        public int? Amount { get; set; }

        public int? BrokerIc { get; set; }

        public bool? Contracted { get; set; }

        public int? MonthlyPayment { get; set; }

        public virtual User User { get; set; }
    }
}
