namespace bigbank_api.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Broker
    {
        /// <summary>
        /// Get and set methods
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Ic { get; set; }

        [StringLength(30)]
        public string Title { get; set; }

        public DateTime? DateStart { get; set; }

        public bool? ContractEnd { get; set; }

        [StringLength(30)]
        public string DataFile { get; set; }
    }
}
