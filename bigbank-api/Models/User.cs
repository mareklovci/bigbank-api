namespace bigbank_api.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Class User
    /// </summary>
    public partial class User
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            ContactAttempts = new HashSet<ContactAttempt>();
            Loans = new HashSet<Loan>();
        }


        /// <summary>
        /// Get and set methods
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PhoneNumber { get; set; }

        [StringLength(12)]
        public string BirthNumber { get; set; }

        [StringLength(20)]
        public string FirstName { get; set; }

        [StringLength(30)]
        public string Surname { get; set; }

        [StringLength(30)]
        public string Email { get; set; }

        [Column(TypeName = "text")]
        public string Note { get; set; }

        /// <summary>
        /// Gets or sets the contact attempts.
        /// </summary>
        /// <value>
        /// The contact attempts.
        /// </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ContactAttempt> ContactAttempts { get; set; }


        /// <summary>
        /// Gets or sets the loans.
        /// </summary>
        /// <value>
        /// The loans.
        /// </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Loan> Loans { get; set; }
    }
}
