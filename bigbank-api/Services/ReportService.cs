﻿using bigbank_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bigbank_api.Services
{
    /// <summary>
    /// Class ReportService
    /// </summary>
    public class ReportService
    {
        private BigBankApi context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportService"/> class.
        /// </summary>
        public ReportService()
        {
            context = new BigBankApi();
        }

        /// <summary>
        /// Gets the broker statistic (Contacting users)
        /// </summary>
        /// <returns>Broker Stats</returns>
        public List<BrokerStat> GetUserReport()
        {
            List<BrokerStat> brokerStats = new List<BrokerStat>();
            foreach (var broker in context.Brokers)
            {
                var stat = new BrokerStat
                {
                    BrokerName = broker.Title,
                    CountOfContactAttempts = GetCountOfContactAttempts(broker),
                    CountOfSuccessfulAttempts = GetCountOfSuccessfulAttempts(broker),
                    CountOfUnsuccessfulAttempts = GetCountOfUnsuccessfulAttempts(broker)
                };
                brokerStats.Add(stat);
            }

            return brokerStats;
        }


        /// <summary>
        /// Get Count Of Contact Attempts
        /// </summary>
        /// <param name="broker"></param>
        /// <returns>Contact Attempts</returns>
        //Počet pokusů o kontakt
        #region GetUserReport statistics methods
        private int GetCountOfContactAttempts(Broker broker)
        {
            int contactAttemptCount = 0;
            foreach (var loan in context.Loans)
            {
                foreach (var contactAttemp in context.ContactAttempts)
                {
                    if (loan.BrokerIc == broker.Ic && contactAttemp.LoanId == loan.Id)
                        contactAttemptCount++;
                }
            }
            return contactAttemptCount;
        }

        /// <summary>
        /// Get Count Of Successful Attempts (Broker)
        /// </summary>
        /// <param name="broker">The broker.</param>
        /// <returns></returns>
        //Získejte počet úspěšných pokusů
        private int GetCountOfSuccessfulAttempts(Broker broker)
        {
            int successfulAttemptCount = 0;
            foreach (var loan in context.Loans)
            {
                foreach (var contactAttemp in context.ContactAttempts)
                {
                    if (loan.BrokerIc == broker.Ic && contactAttemp.LoanId == loan.Id && (contactAttemp.ContactResult == 2 || contactAttemp.ContactResult == 3))
                        successfulAttemptCount++;
                }
            }
            return successfulAttemptCount;
        }

        /// <summary>
        /// Get Count Of Unsuccessful Attempts (Broker)
        /// </summary>
        /// <param name="broker">The broker.</param>
        /// <returns></returns>
        //Získejte počet neúspěšných pokusů
        private int GetCountOfUnsuccessfulAttempts(Broker broker)
        {
            int successfulAttemptCount = 0;
            foreach (var loan in context.Loans)
            {
                foreach (var contactAttemp in context.ContactAttempts)
                {
                    if (loan.BrokerIc == broker.Ic && contactAttemp.LoanId == loan.Id && contactAttemp.ContactResult == 1)
                        successfulAttemptCount++;
                }
            }
            return successfulAttemptCount;
        }
        #endregion

        /// <summary>
        /// Gets the loan report.
        /// </summary>
        /// <returns></returns>
        public List<UserStat> GetLoanReport()
        {
            List<UserStat> brokerStats = new List<UserStat>();

            foreach (var broker in context.Brokers)
            {
                var stat = new UserStat
                {
                    BrokerName = broker.Title,
                    CountOfUsers = GetCountOfUsers(broker),
                    CountOfLoans = GetCountOfLoans(broker)
                };
                brokerStats.Add(stat);
            }

            return brokerStats;
        }

        #region GetLoanReport statistics methods
        private int GetCountOfUsers(Broker broker)
        {
            int userCount = 0;
            foreach (var loan in context.Loans)
            {
                if (loan.BrokerIc == broker.Ic)
                {
                    userCount++;
                }
            }
            return userCount;
        }

        private int GetCountOfLoans(Broker broker)
        {
            int loanCount = 0;
            foreach (var loan in context.Loans)
            {
                if (loan.BrokerIc == broker.Ic && loan.Contracted == true)
                {
                    loanCount++;
                }
            }
            return loanCount;
        }
        #endregion

        /// <summary>
        /// Gets the user age groups.
        /// </summary>
        /// <returns>Age Report</returns>
        public AgeReport GetUserAgeGroups()
        {
            var stat = new AgeReport
            {
                Age18_25 = GetAgeGroupCount(18, 25),
                Age26_35 = GetAgeGroupCount(26, 35),
                Age36_50 = GetAgeGroupCount(36, 50),
                Age51plus = GetAgeGroupCount(51, 99)
            };
            return stat;
        }

        /// <summary>
        /// Age group count.
        /// </summary>
        /// <param name="agefrom">The agefrom.</param>
        /// <param name="ageTo">The age to.</param>
        private int GetAgeGroupCount(int agefrom, int ageTo)
        {
            int ageCount = 0;
            foreach (var user in context.Users)
            {
                foreach (var loan in context.Loans)
                {
                    if (loan.Contracted == true && loan.UserId == user.PhoneNumber)
                    {
                        // birthnumber - yymmdd
                        DateTime birthDate = BirthNumberToDateTime(user.BirthNumber);
                        int age = DateTime.Today.Year - birthDate.Year;

                        if (age >= agefrom && age <= ageTo)
                        {
                            ageCount++;
                        }
                    }
                }
            }
            return ageCount;
        }

        /// <summary>
        /// Birthes the number to date time.
        /// </summary>
        /// <param name="birthNumber">The birth number.</param>
        /// <returns>Date Time</returns>
        private DateTime BirthNumberToDateTime(string birthNumber)
        {
            int year = Int16.Parse(birthNumber.Substring(0, 2));
            int month = Int16.Parse(birthNumber.Substring(2, 2));
            int day = Int16.Parse(birthNumber.Substring(4, 2));

            // I would throw an exception if bithNumber were invalid, but i need this to work now
            if (month > 12)
            {
                month -= 50;
            }
            if (day > 31)
            {
                day = 28;
            }

            int todayYear = DateTime.Today.Year;

            if (year + 2000 <= todayYear)
            {
                year += 2000;
            }
            else
            {
                year += 1900;
            }
            return new DateTime(year, month, day);
        }
    }

    /// <summary>
    /// Get and set methods for BrokerStat
    /// </summary>
    public struct BrokerStat
    {
        public string BrokerName { get; set; }

        public int CountOfContactAttempts { get; set; }

        public int CountOfSuccessfulAttempts { get; set; }

        public int CountOfUnsuccessfulAttempts { get; set; }
    }

    /// <summary>
    /// Get and set methods for UserStat
    /// </summary>
    public struct UserStat
    {
        public string BrokerName { get; set; }

        public int CountOfUsers { get; set; }

        public int CountOfLoans { get; set; }
    }

    /// <summary>
    /// Get and set methods for AgeReport
    /// </summary>
    public struct AgeReport
    {
        public int Age18_25 { get; set; }
        public int Age26_35 { get; set; }
        public int Age36_50 { get; set; }
        public int Age51plus { get; set; }
    }
}