﻿using bigbank_api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace bigbank_api.Services
{
    /// <summary>
    /// Broker Service
    /// </summary>
    public class BrokerService
    {
        private BigBankApi context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerService"/> class.
        /// </summary>
        public BrokerService()
        {
            context = new BigBankApi();
        }

        /// <summary>
        /// Gets the brokers.
        /// </summary>
        /// <returns>All Brokers in DB</returns>
        public List<Broker> GetBrokers()
        {
            return context.Brokers.ToList();
        }

        /// <summary>
        /// Adds the new broker.
        /// </summary>
        /// <param name="broker">The broker.</param>
        /// <returns>New Broker</returns>
        public StreamContent AddBroker(Broker broker)
        {
            context.Brokers.Add(broker);
            context.SaveChanges();

            string path = System.AppDomain.CurrentDomain.BaseDirectory + "/hello-world.pdf";

            return new StreamContent(ReturnFile(path));
        }

        /// <summary>
        /// Updates the broker.
        /// </summary>
        /// <param name="broker">The broker.</param>
        /// <returns>New Broker</returns>
        public Broker UpdateBroker(Broker broker)
        {
            context.Brokers.Remove(context.Brokers.First(x => x.Ic == broker.Ic));
            context.Brokers.Add(broker);
            context.SaveChanges();
            return broker;
        }

        private MemoryStream ReturnFile(string filePath)
        {
            //converting Pdf file into bytes array  
            var dataBytes = File.ReadAllBytes(filePath);

            //adding bytes to memory stream   
            var dataStream = new MemoryStream(dataBytes);

            return dataStream;
        }
    }
}