﻿using bigbank_api.Models;
using System.Linq;
using System.Collections.Generic;

namespace bigbank_api.Controllers
{
    /// <summary>
    /// Class USerService
    /// </summary>
    public class UserService
    {
        private BigBankApi context;

        public UserService()
        {
            context = new BigBankApi();
        }

        /// <summary>
        /// Adds the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>New User</returns>
        public User AddUser(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
            return user;
        }

        /// <summary>
        /// Contacts the user.
        /// </summary>
        /// <param name="ContactAttempt">The contact attempt.</param>
        /// <returns>Contact Attempt</returns>
        public ContactAttempt ContactUser(ContactAttempt ContactAttempt)
        {
            context.ContactAttempts.Add(ContactAttempt);
            context.SaveChanges();
            return ContactAttempt;
        }

        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>User</returns>
        public User UpdateUser(User user)
        {
            //context.Users.Remove(user.PhoneNumber);
            context.Users.Remove(context.Users.First(s => s.PhoneNumber == user.PhoneNumber));
            context.Users.Add(user);
            context.SaveChanges();
            return user;
        }

        /// <summary>
        /// Adds the loan.
        /// </summary>
        /// <param name="loan">The loan.</param>
        /// <returns>NEw Loan</returns>
        public Loan AddLoan(Loan loan)
        {
            context.Loans.Add(loan);
            context.SaveChanges();
            return loan;
        }

        /// <summary>
        /// Updates the loan.
        /// </summary>
        /// <param name="loan">The loan.</param>
        /// <returns>Loan</returns>
        public Loan UpdateLoan(Loan loan)
        {
            int MonthlyPayment = CalculatePayment((int)(loan.Amount), (long)(loan.DueDate));
            var result = context.Loans.SingleOrDefault(l => l.Id == loan.Id);
            if (result != null)
            {
                result.MonthlyPayment = MonthlyPayment;
                result.Contracted = true;
                context.SaveChanges();
            }
            return loan;
        }

        /// <summary>
        /// Gets all not contacted users.
        /// </summary>
        /// <returns>Not Contacted Users</returns>
        public List<User> GetAllNotContactedUsers()
        {
            List<User> notContacted = new List<User>();

            foreach (User user in context.Users)
            {
                if (user.ContactAttempts == null || user.ContactAttempts.Count == 0)
                {
                    notContacted.Add(UserToAdd(user));
                }
                else
                {
                    bool contacted = false;
                    foreach (var contactAttempt in user.ContactAttempts)
                    {
                        if (contactAttempt.ContactResult != 1)
                        {
                            contacted = true;
                        }
                    }
                    if (contacted)
                    {
                        notContacted.Add(UserToAdd(user));
                    }
                }
            }
            return notContacted;
        }

        /// <summary>
        /// Gets the monthly payment.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Mounthly Payment</returns>
        public Loan GetMonthlyPayment(int id)
        {
            Loan loan = context.Loans.FirstOrDefault(l => l.Id == id);

            Loan newLoan = new Loan();
            newLoan.Id = loan.Id;
            newLoan.DueDate = loan.DueDate;
            newLoan.UserId = loan.UserId;
            newLoan.Amount = loan.Amount;
            newLoan.BrokerIc = loan.BrokerIc;
            newLoan.Contracted = loan.Contracted;
            newLoan.MonthlyPayment = loan.MonthlyPayment;

            return newLoan;
        }

        /// <summary>
        /// Users to add.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>Users</returns>
        private User UserToAdd(User user)
        {
            User returnUser = new User
            {
                PhoneNumber = user.PhoneNumber,
                BirthNumber = user.BirthNumber,
                FirstName = user.FirstName,
                Surname = user.Surname,
                Email = user.Email,
                Note = user.Note
            };
            return returnUser;
        }

        /// <summary>
        /// Calculates the payment.
        /// </summary>
        /// <param name="Amount">The amount.</param>
        /// <param name="DueDate">The due date.</param>
        /// <returns>Payment</returns>
        public int CalculatePayment(int Amount, long DueDate)
        {
            return unchecked((int)((Amount * 1.2) / DueDate));
        }
    }
}