﻿using bigbank_api.Models;
using bigbank_api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace bigbank_api.Controllers
{
    /// <summary>
    /// Controller for communuciation between Broker.cs and BrokerService.cs
    /// </summary>
    [RoutePrefix("api/broker")]
    public class BrokerController : ApiController
    {
        private BrokerService brokerService = new BrokerService();

        /// <summary>
        /// Adds new BROKERS.
        /// </summary>
        /// <param name="broker">The broker.</param>
        /// <returns>brokers</returns>
        [HttpPost]
        [Route("add")]
        public HttpResponseMessage AddBroker(Broker broker)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            httpResponseMessage.Content = brokerService.AddBroker(broker);
            httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            httpResponseMessage.Content.Headers.ContentDisposition.FileName = "hello-world.pdf";
            httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
            return httpResponseMessage;
        }

        /// <summary>
        /// Updates BROKERS.
        /// </summary>
        /// <param name="broker">The broker.</param>
        /// <returns>brokers</returns>
        [HttpPost]
        [Route("update")]
        public Broker UpdateBroker(Broker broker)
        {
            return brokerService.UpdateBroker(broker);
        }

        /// <summary>
        /// Gets all BROKERS from DB
        /// </summary>
        /// <returns>brokers</returns>
        [HttpGet]
        public IEnumerable<Broker> GetAllBrokers()
        {
            return brokerService.GetBrokers();
        }

    }
}
