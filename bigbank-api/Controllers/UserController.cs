﻿using bigbank_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace bigbank_api.Controllers
{
    /// <summary>
    /// Controller for communuciation between User.cs and User Service.cs
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private UserService userService = new UserService();

        /// <summary>
        /// Add new user (Registration)
        /// </summary>
        /// <param name="user"></param>
        /// <returns>New user (Registrated)</returns>
        //Registrace nového uživatele
        [HttpPost]
        [Route("add-user")]
        public IHttpActionResult AddUser(User user)
        {
            userService.AddUser(user);
            return Ok();
        }

        /// <summary>
        /// Add new user whe we can contact him + fill the data from new user
        /// </summary>
        /// <param name="user"></param>
        /// <returns>new users</returns>
        //Založení uživatele při kontaktování + vyplnění údajů uživatele
        [HttpPost]
        [Route("update-user")]
        public IHttpActionResult UpdateUser(User user)
        {
            userService.UpdateUser(user);
            return Ok();
        }

        /// <summary>
        /// Contact User
        /// </summary>
        /// <param name="ContactAttempt"></param>
        /// <returns></returns>
        //Založení uživatele při kontaktování + vyplnění údajů uživatele
        [HttpPost]
        [Route("contact-user")]
        public IHttpActionResult ContactUser(ContactAttempt ContactAttempt)
        {
            userService.ContactUser(ContactAttempt);
            return Ok();
        }

        /// <summary>
        /// Registrated and credit calculation on the calculator
        /// </summary>
        /// <param name="loan"></param>
        /// <returns>Loan</returns>
        //Zaregistrování výpočtu úvěru na kalkulačce
        [HttpPost]
        [Route("register-loan")]
        public IHttpActionResult CreateLoan(Loan loan)
        {
            if (loan.Amount < 20000 || loan.Amount > 500000)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Amount must be between 20 000 and 500 000");
            }
            else if(loan.DueDate < 6 || loan.DueDate > 96)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Due date must be between 6 and 96 months");
            }
            else
            {
                userService.AddLoan(loan);
                return Ok();
            }
        }

        /// <summary>
        /// Added the new Loan
        /// </summary>
        /// <param name="loan"></param>
        /// <returns>new loan</returns>
        //Založení nového úvěru
        [HttpPost]
        [Route("create-loan")]
        public IHttpActionResult UpdateLoan(Loan loan)
        {
            if (loan.Amount >= 20000 && loan.Amount <= 500000)
            {
                userService.UpdateLoan(loan);
                return Ok();
            }
            else
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Amount must be between 20 000 and 500 000");
            }           
        }

        /// <summary>
        /// GetAllNotContactedUsers - List with all users (not contacted)
        /// </summary>
        /// <returns>list users - not contacted</returns>
        //List všech uživatelů, kteří nebyli kontaktování
        [Route("notcontacted")]
        public IEnumerable<User> GetAllNotContactedUsers()
        {
            return userService.GetAllNotContactedUsers();
        }

        /// <summary>
        /// Get Monthly Payment
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns mounthly payment (JSON)</returns>
        //Vrátí výši měsíčních splátek JSON, [HttpGet], [Route("get-payment")]
        public Object GetMonthlyPayment(int id)
        {
            return userService.GetMonthlyPayment(id);
        }
    }
}
