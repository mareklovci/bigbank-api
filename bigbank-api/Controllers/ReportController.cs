﻿using bigbank_api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace bigbank_api.Controllers
{
    /// <summary>
    /// Controller for communuciation with ReportService
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [RoutePrefix("api/report")]
    public class ReportController : ApiController
    {
        private readonly ReportService reportService = new ReportService();

        /// <summary>
        /// GetIndex
        /// </summary>
        /// <returns>report</returns>
        public IHttpActionResult GetIndex()
        {
            return NotFound();
        }

        /// <summary>
        /// Report Generated (the most incoming reports)
        /// </summary>
        /// <returns>The most incoming reports</returns>
        //Generování reportů, od kterých partnerů přichází nejvíce reportů.
        [Route("loans")]
        [HttpGet]
        public IEnumerable<UserStat> GetLoanReport()
        {
            return reportService.GetLoanReport();
        }

        /// <summary>
        /// Get User Report
        /// </summary>
        /// <returns>How we can contact our potential clients</returns>
        //Jak se daří či případně nedaří kontaktovat klient
        [Route("client-contact")]
        public IEnumerable<BrokerStat> GetUserReport()
        {
            return reportService.GetUserReport();
        }

        /// <summary>
        /// Gets the user age groups.
        /// </summary>
        /// <returns>Age from user group</returns>
        //Vrátí věk
        [Route("age")]
        public AgeReport GetUserAgeGroups()
        {
            return reportService.GetUserAgeGroups();
        }
    }
}
